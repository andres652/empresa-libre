//~ Empresa Libre
//~ Copyright (C) 2016-2018  Mauricio Baeza Servin (web@correolibre.net)
//~
//~ This program is free software: you can redistribute it and/or modify
//~ it under the terms of the GNU General Public License as published by
//~ the Free Software Foundation, either version 3 of the License, or
//~ (at your option) any later version.
//~
//~ This program is distributed in the hope that it will be useful,
//~ but WITHOUT ANY WARRANTY; without even the implied warranty of
//~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//~ GNU General Public License for more details.
//~
//~ You should have received a copy of the GNU General Public License
//~ along with this program.  If not, see <http://www.gnu.org/licenses/>.


var toolbar_banco = [
    {view: 'richselect', id: 'lst_cuentas_banco', label: 'Cuenta',
        labelWidth: 100, options: []},
    {view: 'text', id: 'txt_cuenta_moneda', label: 'Moneda', readonly: true},
    {view: 'currency2', id: 'txt_cuenta_saldo', label: 'Saldo', readonly: true,
        inputAlign: 'right', value: 0}
]


var toolbar_filtro_cuenta = [
    {view: 'richselect', id: 'filter_cuenta_year', label: 'Año',
        labelAlign: 'right', labelWidth: 50, width: 150, options: []},
    {view: 'richselect', id: 'filter_cuenta_month', label: 'Mes',
        labelAlign: 'right', labelWidth: 50, width: 200, options: months},
    {view: 'daterangepicker', id: 'filter_cuenta_dates', label: 'Fechas',
        labelAlign: 'right', width: 300},
    {},
]


var toolbar_movimientos_banco = [
    {view: 'button', id: 'cmd_agregar_retiro', label: 'Retiro',
        type: 'iconButton', autowidth: true, icon: 'minus'},
    {view: 'button', id: 'cmd_agregar_deposito', label: 'Depósito',
        type: 'iconButton', autowidth: true, icon: 'plus'},
    {},
    {view: 'button', id: 'cmd_complemento_pago', label: 'Generar Factura de Pago',
        type: 'iconButton', autowidth: true, icon: 'file-code-o'},
    {view: 'button', id: 'cmd_show_invoice_pay', label: 'Ver Facturas de Pago',
        type: 'iconButton', autowidth: true, icon: 'table'},
    {},
    {view: 'button', id: 'cmd_cancelar_movimiento', label: 'Cancelar',
        type: 'iconButton', autowidth: true, icon: 'ban'},
]


var grid_cuentabanco_cols = [
    {id: 'id', header:'ID', hidden: true},
    {id: 'fecha', header: 'Fecha', width: 150},
    {id: 'numero_operacion', header: 'Referencia'},
    {id: 'way_payment', header: 'Forma de Pago', hidden: true},
    {id: 'descripcion', header: ['Descripción', {content: 'textFilter'}],
        fillspace:true},
    {id: 'retiro', header: ['Retiro', {content: 'numberFilter'}],
        width: 125, format: webix.i18n.priceFormat, css: 'right'},
    {id: 'deposito', header: ['Depósito', {content: 'numberFilter'}],
        width: 125, format: webix.i18n.priceFormat, css: 'right'},
    {id: 'saldo', header: ['Saldo'],
        width: 125, format: webix.i18n.priceFormat, css: 'right'},
    {id: 'invoice', header: ['FP'], width: 40, css: 'center', hidden: true},
]


var grid_cuentabanco = {
    view: 'datatable',
    id: 'grid_cuentabanco',
    select: 'row',
    adjust: true,
    footer: true,
    resizeColumn: true,
    headermenu: true,
    columns: grid_cuentabanco_cols,
}


var grid_cfdi_por_pagar_cols = [
    {id: 'index', header: '#', adjust: 'data', css: 'right'},
    {id: 'id', header: 'ID', hidden: true},
    {id: 'serie', header: 'Serie', adjust: 'data'},
    {id: 'folio', header: ['Folio', {content: 'numberFilter'}], adjust: 'data', css: 'right'},
    {id: 'uuid', header: 'UUID', width: 250, hidden: true},
    {id: 'fecha', header: 'Fecha y Hora', width: 150, sort: 'date'},
    {id: 'tipo_comprobante', header: 'Tipo', adjust: 'data'},
    {id: 'estatus', header: 'Estatus', adjust: 'header'},
    {id: 'cliente', header: ['Razón Social', {content: 'selectFilter'}],
        fillspace:true, sort: 'string'},
    {id: 'total', header: ['Total'], width: 125, sort: 'int',
        format: webix.i18n.priceFormat, css: 'right', hidden: true},
    {id: 'currency', header: ['Moneda', {content: 'selectFilter'}],
        adjust: 'data', sort: 'string', hidden: true},
    {id: 'total_mn', header: ['Total M.N'], width: 125, sort: 'int',
        format: webix.i18n.priceFormat, css: 'right'},
    {id: 'saldo', header: ['Saldo'], width: 125, sort: 'int',
        format: webix.i18n.priceFormat, css: 'right'},
]


var grid_cfdi_este_deposito_cols = [
    {id: 'index', header: '#', adjust: 'data', css: 'right'},
    {id: 'id', header: 'ID', hidden: true},
    {id: 'serie', header: 'Serie', adjust: 'data'},
    {id: 'folio', header: 'Folio', adjust: 'data', css: 'right'},
    {id: 'uuid', header: 'UUID', width: 250, hidden: true},
    {id: 'fecha', header: 'Fecha y Hora', width: 150, sort: 'date'},
    {id: 'tipo_comprobante', header: 'Tipo', adjust: 'data'},
    {id: 'estatus', header: 'Estatus', adjust: 'header'},
    {id: 'cliente', header: ['Razón Social'], fillspace: true},
    {id: 'total', header: ['Total'], width: 125, sort: 'int',
        format: webix.i18n.priceFormat, css: 'right', hidden: true},
    {id: 'currency', header: ['Moneda', {content: 'selectFilter'}],
        adjust: 'data', sort: 'string', hidden: true},
    {id: 'total_mn', header: ['Total M.N'], width: 125, sort: 'int',
        format: webix.i18n.priceFormat, css: 'right'},
    {id: 'saldo', header: ['Saldo'], width: 125, sort: 'int',
        format: webix.i18n.priceFormat, css: 'right'},
    {id: 'this_pay', header: ['Este pago'], width: 125, sort: 'int', hidden: true,
        format: webix.i18n.priceFormat, css: 'right', editor: 'text'},
    {id: 'importe', header: ['Este pago'], width: 125, sort: 'int',
        format: webix.i18n.priceFormat, css: 'right', editor: 'text'},
    {id: 'type_change', header: ['T.C.'], width: 75, hidden: true,
        format: format_currency4, css: 'right'},
]


var grid_cfdi_pay_cols = [
    {id: 'index', header: '#', adjust: 'data', css: 'right',
        footer: {content: 'countRows', colspan: 3, css: 'right'}},
    {id: "id", header:"ID", hidden:true},
    {id: 'serie', header: ["Serie"], adjust: "data", sort: 'string'},
    {id: 'folio', header: ['Folio'], adjust: 'data',
        sort: 'int', css: 'right', footer: {text: 'Facturas', colspan: 3}},
    {id: "uuid", header: ["UUID"], adjust: "data",
        sort:"string", hidden:true},
    {id: "fecha", header: ["Fecha y Hora"],
        adjust: "data", sort: "string"},
    {id: "tipo_comprobante", header: ["Tipo"],
        adjust: 'header', sort: 'string'},
    {id: "estatus", header: ["Estatus"],
        adjust: "data", sort:"string"},
    {id: 'cliente', header: ['Razón Social'], fillspace: true},
    {id: 'xml', header: 'XML', adjust: 'data', template: get_icon('xml')},
    {id: 'pdf', header: 'PDF', adjust: 'data', template: get_icon('pdf')},
    {id: 'email', header: '', adjust: 'data', template: get_icon('email')}
]


var grid_pay_related_cols = [
    {id: 'index', header: '#', adjust: 'data', css: 'right'},
    {id: 'id', header: 'ID', hidden: true},
    {id: 'serie', header: 'Serie', adjust: 'data'},
    {id: 'folio', header: 'Folio', adjust: 'data', css: 'right'},
    {id: 'uuid', header: 'UUID', width: 250, hidden: true},
    {id: 'fecha', header: 'Fecha y Hora', width: 150, sort: 'date'},
    {id: 'tipo_comprobante', header: 'Tipo', adjust: 'data'},
    {id: 'estatus', header: 'Estatus', adjust: 'header'},
    {id: 'cliente', header: ['Razón Social'], fillspace: true},
    {id: 'total', header: ['Total'], width: 125, sort: 'int',
        format: webix.i18n.priceFormat, css: 'right'},
    {id: 'saldo', header: ['Saldo'], width: 125, sort: 'int',
        format: webix.i18n.priceFormat, css: 'right', css: 'right',
        footer: 'Importe Depósito'},
    {id: 'importe', header: ['Este pago'], width: 125, sort: 'int',
        format: webix.i18n.priceFormat, css: 'right',
        footer: {content: 'summColumn', css: 'right'}},
]


var grid_cfdi_por_pagar = {
    view: 'datatable',
    id: 'grid_cfdi_por_pagar',
    select: 'row',
    multiselect: true,
    autoConfig: false,
    adjust: true,
    height: 250,
    resizeColumn: true,
    headermenu: true,
    drag: true,
    columns: grid_cfdi_por_pagar_cols,
    on:{
        'data->onStoreUpdated':function(){
            this.data.each(function(obj, i){
                obj.index = i + 1
            })
        }
    },
}


var grid_cfdi_este_deposito = {
    view: 'datatable',
    id: 'grid_cfdi_este_deposito',
    select: 'row',
    multiselect: true,
    autoConfig: false,
    adjust: true,
    height: 200,
    resizeColumn: true,
    headermenu: true,
    drag: true,
    editable: true,
    columns: grid_cfdi_este_deposito_cols,
    on:{
        'data->onStoreUpdated':function(){
            this.data.each(function(obj, i){
                obj.index = i + 1
            })
        }
    },
}


var grid_cfdi_pay = {
    view: 'datatable',
    id: 'grid_cfdi_pay',
    select: 'row',
    autoConfig: false,
    adjust: true,
    autoheight: true,
    resizeColumn: true,
    headermenu: true,
    columns: grid_cfdi_pay_cols,
    on:{
        'data->onStoreUpdated':function(){
            this.data.each(function(obj, i){
                obj.index = i + 1
            })
        }
    },
}


var grid_pay_related = {
    view: 'datatable',
    id: 'grid_pay_related',
    select: 'row',
    autoConfig: false,
    adjust: true,
    autoheight: true,
    resizeColumn: true,
    headermenu: true,
    footer: true,
    columns: grid_pay_related_cols,
    on:{
        'data->onStoreUpdated':function(){
            this.data.each(function(obj, i){
                obj.index = i + 1
            })
        }
    },
}


var grid_bank_invoice_pay_cols = [
    {id: 'index', header: '#', adjust: 'data', css: 'right',
        footer: {content: 'countRows', colspan: 3, css: 'right'}},
    {id: "id", header:"ID", hidden:true},
    {id: 'serie', header: ["Serie"], adjust: "data", sort: 'string',
        template: '{common.subrow()} #serie#'},
    {id: 'folio', header: ['Folio', {content: 'numberFilter'}], adjust: 'header',
        sort: 'int', css: 'right', footer: {text: 'Facturas', colspan: 3}},
    {id: "uuid", header: ["UUID", {content: "textFilter"}], adjust: "data",
        sort:"string", hidden:true},
    {id: "fecha", header: ["Fecha y Hora"], width: 135,
        sort: "string"},
    {id: "tipo_comprobante", header: ["Tipo"], adjust: 'data',
        sort: 'string'},
    {id: "estatus", header: ["Estatus", {content: "selectFilter"}],
        adjust: "header", sort:"string"},
    {id: 'movimiento', header: ['Movimiento', {content: "selectFilter"}],
        adjust: 'data', sort: 'string'},
    {id: 'total', header: ['Total', {content: 'numberFilter'}],
        width: 125, sort: 'int', format: webix.i18n.priceFormat, css: 'right',
        hidden: true},
    {id: 'currency', header: ['Moneda', {content: 'selectFilter'}],
        adjust: 'data', sort: 'string', hidden: true},
    {id: 'total_mn', header: ['Total M.N.', {content: 'numberFilter'}],
        width: 150, sort: 'int', format: webix.i18n.priceFormat, css: 'right',
        footer: {content: 'summTimbrada', css: 'right'}},
    {id: 'cliente', header: ['Razón Social', {content: 'selectFilter'}],
        fillspace: true, sort: 'string'},
    {id: 'xml', header: 'XML', adjust: 'data', template: get_icon('xml')},
    {id: 'pdf', header: 'PDF', adjust: 'data', template: get_icon('pdf')},
    //~ {id: 'ods', header: 'ODS', adjust: 'data', template: get_icon('table')},
    //~ {id: 'zip', header: 'ZIP', adjust: 'data', template: get_icon('zip')},
    {id: 'email', header: '', adjust: 'data', template: get_icon('email')}
]


var grid_bank_invoice_pay = {
    view: 'datatable',
    id: 'grid_bank_invoice_pay',
    //~ subview: sv_grid_invoices,
    select: 'row',
    adjust: true,
    footer: true,
    resizeColumn: true,
    headermenu: true,
    columns: grid_bank_invoice_pay_cols,
    scheme:{
        $change:function(item){
            if (item.estatus == 'Cancelada'){
                item.$css = 'cancel'
            }
        }
    },
    on:{
        'data->onStoreUpdated':function(){
            this.data.each(function(obj, i){
                obj.index = i + 1
            })
        }
    },
}


var toolbar_banco_retiro = [
    {view: 'label', label: 'Agregar retiro de banco', id: 'title_bank_retiro'},
    {view: 'button', id: 'cmd_guardar_retiro', label: 'Guardar Retiro',
        type: 'iconButton', autowidth: true, icon: 'minus'},
    {view: 'icon', click: '$$("multi_bancos").setValue("banco_home")',
        icon: 'times-circle'}
]


var toolbar_banco_deposito = [
    {view: 'label', label: 'Agregar depósito de banco', id: 'title_bank_deposit'},
    {view: 'button', id: 'cmd_guardar_deposito', label: 'Guardar Depósito',
        type: 'iconButton', autowidth: true, icon: 'plus'},
    {view: 'icon', click: '$$("multi_bancos").setValue("banco_home")',
        icon: 'times-circle'}
]


var toolbar_bank_pay = [
    {view: 'label', label: 'Factura de pago'},
    {},
    {view: 'button', id: 'cmd_pay_stamp', label: 'Timbrar',
        type: 'iconButton', autowidth: true, icon: 'ticket'},
    {view: 'button', id: 'cmd_pay_cancel', label: 'Cancelar',
        type: 'iconButton', autowidth: true, icon: 'minus'},
    {view: 'button', id: 'cmd_pay_delete', label: 'Eliminar',
        type: 'iconButton', autowidth: true, icon: 'ban'},
    {},
    {view: 'icon', click: '$$("multi_bancos").setValue("banco_home")',
        icon: 'times-circle'}
]


var toolbar_bank_invoice_pay = [
    {view: 'label', label: 'Administración de Facturas de Pago'},
    {},
    {view: 'icon', click: '$$("multi_bancos").setValue("banco_home")',
        icon: 'times-circle'}
]


var toolbar_bank_invoice_pay_filter = [
    {view: 'richselect', id: 'filter_invoice_pay_year', label: 'Año',
        labelAlign: 'right', labelWidth: 50, width: 150, options: []},
    {view: 'richselect', id: 'filter_invoice_pay_month', label: 'Mes',
        labelAlign: 'right', labelWidth: 50, width: 200, options: months},
    {view: 'daterangepicker', id: 'filter_invoice_pay_dates', label: 'Fechas',
        labelAlign: 'right', width: 300},
    {},
]


var controls_banco_retiro = [
    {view: 'toolbar', elements: toolbar_banco_retiro},
    {cols: [
        {view: 'datepicker', id: 'date_retiro', name: 'retiro_fecha',
            label: 'Fecha', format: '%d-%M-%Y', labelAlign: 'right',
            required: true, invalidMessage: 'Selecciona una fecha',
            labelWidth: 125},
        {view: 'search', id: 'time_retiro', name: 'retiro_hora',
            label: 'Hora', icon: 'clock-o', labelAlign: 'right',
            pattern:{mask: '##:##:##', allow:/[0-9]/g}, required: true,
            invalidMessage: 'Captura una hora'},
        {view: 'text', id: 'retiro_referencia', name: 'retiro_referencia',
            label: 'Referencia', labelAlign: 'right'},
    ]},
    {cols: [
        {view: 'richselect', id: 'lst_retiro_forma_pago',
            name: 'retiro_forma_pago', label: 'Forma de Pago', required: true,
            options: [], labelWidth: 125, labelAlign: 'right'},
        {view: 'currency2', type: 'text', id: 'txt_retiro_importe',
            name: 'retiro_importe', label: 'Importe', labelAlign: 'right',
            required: true, invalidMessage: 'Captura un valor númerico',
            inputAlign: 'right', value: ''}
    ]},
    {cols: [
        {view: 'text', id: 'retiro_descripcion', name: 'retiro_descripcion',
            label: 'Descripción', labelAlign: 'right', required: true,
            labelWidth: 125},
    ]},
]


var controls_banco_deposito = [
    {view: 'toolbar', elements: toolbar_banco_deposito},
    {cols: [
        {view: 'datepicker', id: 'date_deposito', name: 'deposito_fecha',
            label: 'Fecha', format: '%d-%M-%Y', labelAlign: 'right',
            required: true, invalidMessage: 'Selecciona una fecha',
            labelWidth: 125},
        {view: 'search', id: 'time_deposito', name: 'deposito_hora',
            label: 'Hora', icon: 'clock-o', labelAlign: 'right',
            pattern:{mask: '##:##:##', allow:/[0-9]/g}, required: true,
            invalidMessage: 'Captura una hora'},
        {view: 'text', id: 'deposito_referencia', name: 'deposito_referencia',
            label: 'Referencia', labelAlign: 'right'},
    ]},
    {cols: [
        {view: 'richselect', id: 'lst_deposito_forma_pago',
            name: 'deposito_forma_pago', label: 'Forma de Pago', required: true,
            options: [], labelWidth: 125, labelAlign: 'right'},
        {view: 'currency2', type: 'text', id: 'txt_deposito_importe',
            name: 'deposito_importe', label: 'Importe', labelAlign: 'right',
            required: true, invalidMessage: 'Captura un valor númerico',
            inputAlign: 'right', value: ''},
        {view: 'currency4', type: 'text', id: 'deposit_type_change',
            name: 'deposit_type_change', label: 'T.C.', labelAlign: 'right',
            required: false, invalidMessage: 'Captura un valor númerico',
            inputAlign: 'right', value: '1.0000', width: 175, hidden: true}
    ]},
    {cols: [
        {view: 'textarea', id: 'deposito_descripcion', label: 'Descripción',
            name: 'deposito_descripcion', labelAlign: 'right', required: true,
            labelWidth: 125, height: 70},
    ]},
    {cols: [
        {view: 'label', label: '<b>Facturas por pagar: </b>'},
        {view: 'richselect', id: 'lst_partner_account_bank', hidden: true,
            name: 'partner_account_bank', label: 'Cuenta Origen', required: false,
            options: [], labelWidth: 125, labelAlign: 'right'},
        {},
        {view: 'button', id: 'cmd_invoice_payed', label: 'Solo marcar pagada',
            type: 'iconButton', autowidth: true, icon: 'check-circle',
            tooltip: 'No afecta a saldos'},
    ]},
    grid_cfdi_por_pagar,
    {view: 'label', label: '<b>Facturas a pagar en este depósito: </b>'},
    grid_cfdi_este_deposito,
]


var controls_bank_pay = [
    {view: 'toolbar', elements: toolbar_bank_pay},
    {view: 'label', label: '<b>Este depósito: </b>'},
    {cols: [
        {view: 'datepicker', id: 'pay_date', name: 'pay_date',
            label: 'Fecha', format: '%d-%M-%Y', labelAlign: 'right',
            required: true, invalidMessage: 'Selecciona una fecha',
            labelWidth: 125, readonly: true},
        {view: 'search', id: 'pay_time', name: 'pay_time', label: 'Hora',
            icon: 'clock-o', labelAlign: 'right', required: true,
            readonly: true,
            invalidMessage: 'Captura una hora'},
        {view: 'text', id: 'pay_reference', name: 'pay_reference',
            label: 'Referencia', labelAlign: 'right', readonly: true},
    ]},
    {cols: [
        {view: 'richselect', id: 'pay_way_payment', readonly: true,
            name: 'pay_way_payment', label: 'Forma de Pago', required: true,
            options: [], labelWidth: 125, labelAlign: 'right'},
        {view: 'currency', type: 'text', id: 'pay_import', name: 'pay_import',
            label: 'Importe', labelAlign: 'right', required: true, readonly: true,
            invalidMessage: 'Captura un valor númerico', inputAlign: 'right'}
    ]},
    {cols: [
        {view: 'textarea', id: 'pay_description', label: 'Descripción',
            name: 'pay_description', labelAlign: 'right', required: true,
            labelWidth: 125, height: 70, readonly: true},
    ]},
    {view: 'label', label: '<b>Facturas de pago de este depósito: </b>'},
    grid_cfdi_pay,
    {view: 'label', label: '<b>Facturas relacionadas en este pago: </b>'},
    grid_pay_related
]


var controls_bank_invoice_pay = [
    {view: 'toolbar', elements: toolbar_bank_invoice_pay},
    {view: 'toolbar', elements: toolbar_bank_invoice_pay_filter},
    grid_bank_invoice_pay
]


var form_banco_retiro = {
    type: 'space',
    responsive: true,
    cols: [{
        view: 'form',
        id: 'form_banco_retiro',
        complexData: true,
        scroll: true,
        elements: controls_banco_retiro,
    }],
}


var form_banco_deposito = {
    type: 'space',
    responsive: true,
    cols: [{
        view: 'form',
        id: 'form_banco_deposito',
        complexData: true,
        scroll: true,
        elements: controls_banco_deposito,
    }],
}


var form_bank_pay = {
    type: 'space',
    responsive: true,
    cols: [{
        view: 'form',
        id: 'form_bank_pay',
        complexData: true,
        scroll: true,
        elements: controls_bank_pay,
    }],
}


var form_bank_invoice_pay = {
    type: 'space',
    responsive: true,
    cols: [{
        view: 'form',
        id: 'form_bank_invoice_pay',
        complexData: true,
        scroll: true,
        elements: controls_bank_invoice_pay,
    }],
}


var multi_bancos = {
    id: 'multi_bancos',
    animate: true,
    cells:[
        {id: 'banco_home', rows:[
            {view: 'toolbar', elements: toolbar_banco},
            {view: 'toolbar', elements: toolbar_filtro_cuenta},
            {view: 'toolbar', elements: toolbar_movimientos_banco},
            grid_cuentabanco,
        ]},
        {id: 'banco_retiro', rows: [form_banco_retiro]},
        {id: 'banco_deposito', rows: [form_banco_deposito]},
        {id: 'bank_pay', rows: [form_bank_pay]},
        {id: 'bank_invoice_pay', rows: [form_bank_invoice_pay]}
    ],
}


var title_partners = 'Administración de Bancos'
var app_bancos = {
    id: 'app_bancos',
    rows:[
        {view: 'template', id: 'th_bancos', type: 'header',
            template: title_partners},
        multi_bancos
    ]
}


var win_body_mov_description = {rows: [
    {minHeight: 5, maxHeight: 5},
    {view: 'textarea', id: 'mov_description', name: 'mov_description',
        height: 300},
    {minHeight: 5, maxHeight: 5},
    {cols: [{},
        {view: 'button', id: 'cmd_save_mov_description', autowidth: true,
            label: 'Guardar', type: 'iconButton', icon: 'save', hotkey: 'Ctrl+enter'},
        {maxWidth: 50},
        {view: 'button', id: 'cmd_close_mov_description', autowidth: true,
            label: 'Cerrar', type: 'iconButton', icon: 'times-circle', hotkey: 'esc'},
        {}]},
    {minHeight: 5, maxHeight: 5},
],}


var win_mov_description = {
    init: function(){
        webix.ui({
            view: 'window',
            id: 'win_mov_description',
            height: 400,
            width: 600,
            modal: true,
            position: 'center',
            head: 'Descripción del movimiento',
            body: win_body_mov_description,
        })
        $$('cmd_save_mov_description').attachEvent('onItemClick', cmd_save_mov_description_click)
        $$('cmd_close_mov_description').attachEvent('onItemClick', cmd_close_mov_description_click)
    }
}
